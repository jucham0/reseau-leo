# TP Réseau - Première Exploration

## I. Exploration locale en solo 🕵️‍♂️

### Infos réseau 🌐

- **Ethernet**
  - Nom: Realtek Gaming 2.5GbE
  - MAC: D8-BB-C1-D7-7F-5B
  - Statut: Déconnecté

- **Wi-Fi**
  - Nom: Intel(R) Wi-Fi 6 AX201
  - MAC: D4-54-8B-A1-73-0E
  - IP: 192.168.1.73
  - Sous-réseau: 255.255.255.0
  - Gateway: 192.168.1.1
  - DHCP: 192.168.1.1
  - DNS: 192.168.1.1

### Passerelle par défaut 🚧

- **Gateway**: 192.168.1.1

### MAC de la passerelle 🔍

- **Gateway MAC**: d4-f8-29-57-19-00

## II. Exploration en duo (Non réalisé) 🚫

Raisons techniques - Pas de câble RJ45.

## III. Autres outils/protocoles 🛠️

### DHCP 📝

- **Serveur DHCP**: 192.168.1.1
- **Bail**: 3 nov. 2023 - 6 nov. 2023

### DNS 🌍

- **Serveur**: dns.google (8.8.8.8)
- **Requête `google.com`**: IPv6 + IPv4

## IV. Wireshark - Trafic Capture 📦

Capture pendant `ping google.com`.

- **Fichier** :  `wirescreen.pcapng`
