# I. Configuration de l'adresse IP

## 🌞 Mise en place d'une configuration réseau fonctionnelle entre les deux machines

- Adresses IP choisies :
  - IP 1 : 10.10.10.1
  - IP 2 : 10.10.10.2
- Masque : 255.255.255.252
- Adresse réseau : 10.10.10.0
- Adresse de diffusion : 10.10.10.3

## 🌞 Vérification de la connexion entre les deux machines

```bash
ping 10.10.10.1
```

Résultat du ping 🏓 :
```
Envoi d’une requête 'Ping'  10.10.10.1 avec 32 octets de données :
Réponse de 10.10.10.1 : octets=32 temps=2 ms TTL=128
Réponse de 10.10.10.1 : octets=32 temps=2 ms TTL=128
Réponse de 10.10.10.1 : octets=32 temps=2 ms TTL=128
Réponse de 10.10.10.1 : octets=32 temps=2 ms TTL=128

Statistiques Ping pour 10.10.10.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 2ms, Maximum = 2ms, Moyenne = 2ms
```

## 🦈 Utilisation de Wireshark pour analyser le type de paquet ICMP envoyé par ping

- 0 : Echo reply (utilisé pour le ping)
- 8 : Echo request (utilisé pour le ping)

# II. ARP my bro

## 🌞 Vérification de la table ARP

```bash
arp -a
```

- Binôme :
  - Interface : 10.10.10.2 --- 0x12
    ```
    Adresse Internet      Adresse physique      Type
    10.10.10.1            08-bf-b8-c2-2a-57     dynamique
    ```

- Gateway :
  - Interface : 10.10.10.2 --- 0x12
    ```
    Adresse Internet      Adresse physique      Type
    10.33.19.254          c8-7Ff-54-cc-00-08     dynamique
    ```

## 🌞 Manipulation de la table ARP

### AVANT LA COMMANDE :

```bash
arp -a
```

Table ARP avant la manipulation :
```
Interface : 10.10.10.2 --- 0x12
  Adresse Internet      Adresse physique      Type
  10.10.10.1            08-bf-b8-c2-2a-57     dynamique
  10.10.10.3            ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  224.77.77.77          01-00-5e-4d-4d-4d     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```

### APRÈS LA COMMANDE:

```bash
arp -d *
```

Table ARP après manipulation :
```
Interface : 10.10.10.2 --- 0x12
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff
```



## Wireshark - Capture 📦

Les 2 captures `🦈 Wireshark`.

- **Fichier 1** :  `TP2PINGARP.pcapng`
- **Fichier 2** :  `TP2PINGPVRES.pcapng`

